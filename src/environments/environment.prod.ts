export const environment = {
  production: true,
  api_offline: false,
  api_url_base: 'https://swapi.dev/api',
  api_imageUrl_base: 'https://starwars-visualguide.com/assets/img/starships/'
};
