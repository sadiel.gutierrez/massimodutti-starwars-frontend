import { of, Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { StarShipsService } from 'src/app/views/starships/starships.service';


const paginated = [];
@Injectable()
export class ShipsServiceMock extends StarShipsService {

    ships = of ([
        {
            id: 1,
            name: '',
            image: '4CF4E759E22A21EDE050F40A3B04004C'
        }
    ]);
}
