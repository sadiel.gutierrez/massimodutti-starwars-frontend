import { Paginated } from 'src/app/core/models/pagination.model';
import { StarShip } from 'src/app/core/models/starship.model';

export const STARSHIPS_DATA_SERVICE_PAGINATED: Paginated<StarShip> = {
    results: [
        {
            name: 'Executor',
            model: 'Executor-class star dreadnought',
            manufacturer: 'Kuat Drive Yards, Fondor Shipyards',
            length: '19000',
            passengers: 38000,
            created: '2014-12-15T12:31:42.547000Z',
            url: 'http://swapi.dev/api/starships/1/'
        }
    ],
    count: 1,
    previous: null,
    next: 'https://swapi.co/api/starships/?page=2'
};
