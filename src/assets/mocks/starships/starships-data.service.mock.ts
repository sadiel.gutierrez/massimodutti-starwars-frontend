import { of } from 'rxjs';
import { Injectable } from '@angular/core';
import { STARSHIPS_DATA_SERVICE_PAGINATED } from './starships-data.mock';
import { StarShipsDataService } from 'src/app/services/starships-data/starships-data.service';


@Injectable()
export class StarShipsDataServiceMock extends StarShipsDataService {

  getAllStarShips(page: number, options: any) {
    return of(STARSHIPS_DATA_SERVICE_PAGINATED);
  }
}
