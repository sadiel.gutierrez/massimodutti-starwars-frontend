import { User } from 'src/app/core/models/user.model';

export const USER: User = {
    id: 1,
    firstName: 'Massimo',
    lastName: 'Dutti',
    userName: 'massimodutti',
    password: '@=e8_R<k9HdU<rdY',
    roles: [
        {
            id: 1,
            code: 'ROLE_ADMIN',
            description: 'Administration Role'
        },
        {
            id: 2,
            code: 'ROLE_GUEST',
            description: 'User Role'
        }
    ]
};
