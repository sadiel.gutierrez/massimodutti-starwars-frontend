import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { StarShipsService } from './starships.service';
import { ActionsEnum } from 'src/app/core/models/enums.model';
import { MatDialog } from '@angular/material/dialog';
import { AddEditStarShipComponent } from './components/add-edit-starship/add-edit-starship.component';
import { StarShipsDataService } from 'src/app/services/starships-data/starships-data.service';
import { CacheService } from 'src/app/services/cache/cache.service';
import { StarShip } from 'src/app/core/models/starship.model';

@Component({
  selector: 'app-starships',
  templateUrl: './starships.component.html',
  styleUrls: ['./starships.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [StarShipsService, StarShipsDataService, CacheService]
})
export class StarShipsComponent implements OnInit {

  constructor(
    public starShipsService: StarShipsService,
    public dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.starShipsService.getAllStarShips();
  }

  doAction(action: string, starShip: StarShip = {}) {
    switch (action) {
      case ActionsEnum.ADD:
        this.openAddStarShipModal();
        break;
      case ActionsEnum.EDIT:
      this.openEditStarShipModal(starShip);
      break;
    }
  }

  /**
   * Display dialog to let user create a new starship
   */
  private openAddStarShipModal() {
    const dialogResult = this.dialog.open(AddEditStarShipComponent, {
      width: '750px',
      data: {}
    });
    dialogResult.afterClosed().subscribe(result => {
      if (result) {
        const ships = [];
        ships.push(result);
        this.starShipsService.starShips.getValue().forEach(x => ships.push(x));
        this.starShipsService.starShips.next(ships);
      }
    });
  }

  /**
   * Display dialog to let user edit current starship
   */
  private openEditStarShipModal(starShip: StarShip = {}) {
    const dialogResult = this.dialog.open(AddEditStarShipComponent, {
      width: '750px',
      data: {starShip}
    });
    dialogResult.afterClosed().subscribe(result => {
      if (result) {
        const ships = [];
        if (this.starShipsService.starShips.getValue().find(x => x.url === result.url)) {
          ships.push(result);
          this.starShipsService.starShips.getValue().filter(x => x.url !== result.url).forEach(x => ships.push(x));
        }
        this.starShipsService.starShips.next(ships);
      }
    });
  }

  onPageChange(page: number) {
    this.starShipsService.currentPage = page;
    this.starShipsService.getAllStarShips(page);
  }

}
