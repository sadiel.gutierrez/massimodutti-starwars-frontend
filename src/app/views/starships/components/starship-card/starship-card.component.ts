import { Component, OnInit, Input } from '@angular/core';
import { StarShip } from 'src/app/core/models/starship.model';

@Component({
  selector: 'app-starship-card',
  templateUrl: './starship-card.component.html',
  styleUrls: ['./starship-card.component.scss']
})
export class StarShipCardComponent implements OnInit {
  /**
   * StarShip to show on card
   */
  @Input() starShip: StarShip;

  constructor() { }

  ngOnInit(): void {
  }

  setUnavailableImage(event: any) {
    event.style.backgroundImage = 'url(assets/images/image_unavailable.png)';
  }

}
