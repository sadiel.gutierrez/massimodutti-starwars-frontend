import { Component, OnInit, Input, ChangeDetectorRef } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { StarShipsService } from '../../starships.service';

@Component({
  selector: 'app-starship-information',
  templateUrl: './starship-information.component.html',
  styleUrls: ['./starship-information.component.scss']
})
export class StarShipInformationComponent implements OnInit {
  /**
   * Form recibed to handle controls
   */
  @Input() form: FormGroup;

  constructor(
    private changeDetectorRef: ChangeDetectorRef,
    public starShipsService: StarShipsService
  ) { }

  ngOnInit(): void {
  }

  /**
   * handle file from browsing
   */
  fileBrowseHandler(files) {
    const reader = new FileReader();
    if (files.length) {
      reader.readAsDataURL(files[0]);
      reader.onloadend = () => {
        let mimeType = '';
        const mimeTypeLength = files[0].type.split('/').length;
        mimeType = files[0].type.split('/')[mimeTypeLength - 1];
        const data = {
          file: {
            value: reader.result,
            name: files[0].name,
            size: files[0].size,
            mimeType,
            blob: files[0]
          }
        };
        this.form.get('information.imageUrl').setValue(data.file.value.toString());
        this.form.get('information.imageSrcBlob').setValue(data.file.blob);
        this.changeDetectorRef.markForCheck();
      };
    }
  }

}
