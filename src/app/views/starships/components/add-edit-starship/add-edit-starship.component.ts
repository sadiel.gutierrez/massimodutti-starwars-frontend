import { Component, OnInit, Inject, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subject, Subscription } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Router } from '@angular/router';
import { StarShip } from 'src/app/core/models/starship.model';
import { StarShipsService } from '../../starships.service';
import { isValidForm } from 'src/app/core/components/helpers/functions';
import { CustomValidators } from 'src/app/core/components/helpers/custom-validators';
import { StarShipsDataService } from 'src/app/services/starships-data/starships-data.service';
import { CacheService } from 'src/app/services/cache/cache.service';

@Component({
  selector: 'app-add-edit-starship',
  templateUrl: './add-edit-starship.component.html',
  styleUrls: ['./add-edit-starship.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [StarShipsService, StarShipsDataService, CacheService]
})
export class AddEditStarShipComponent implements OnInit {

  formSubscription: Subscription[] = [];
  /**
   * Contains subscriptions to destroy and prevent memory leaks
   */
  private destroy$: Subject<void> = new Subject<void>();
  /**
   * Form with his current controls to handle data
   */
  form: FormGroup;
  /**
   *
   * @param dialogRef Dialog ref to close it
   * @param data Modal data
   * @param changeDetectorRef Inject changeDetectorRef to detech changes
   * @param fb Inject FormBuilder to create FormGroups
   */
  constructor(
    public dialogRef: MatDialogRef<AddEditStarShipComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { starShip: StarShip },
    private changeDetectorRef: ChangeDetectorRef,
    private fb: FormBuilder,
    public starShipsService: StarShipsService,
    public router: Router
  ) { }

  ngOnInit(): void {
    this.initForm(this.data.starShip);
  }

  /**
   * Save starship with data on form controls
   */
  saveStarShip() {
    if (isValidForm(this.form)) {
      const req = {
        ...this.form.get('information').value,
        created: new Date().getTime()
      };
      if (!this.data.starShip) {
          this.starShipsService.addStarShip(req).pipe(
            takeUntil(this.destroy$),
          ).subscribe(result => {
            this.dialogRef.close(result);
          });
      } else {
          this.starShipsService.editStarShip(req).pipe(
            takeUntil(this.destroy$),
          ).subscribe(result => {
            this.dialogRef.close(result);
          });
      }
    }
  }

  /**
   * Initialize form with controls
   */
  private initForm(starShip: StarShip = {}) {
    this.form = this.fb.group({
      information: this.fb.group({
        name: [starShip.name, [Validators.required, CustomValidators.minMaxLength({ min: 5, max: 50 })]],
        model: [starShip.model, Validators.required],
        manufacturer: [starShip.manufacturer, Validators.required],
        passengers: [starShip.passengers, Validators.required],
        imageUrl: null,
        imageSrcBlob: null,
        url: starShip.url
      })
    });
    this.changeDetectorRef.markForCheck();
  }

  getStarShip() {
    return new StarShip(
      null,
      this.form.get('information.name').value,
      this.form.get('information.model').value,
      this.form.get('information.manufacturer').value,
      this.form.get('information.passengers').value,
      null,
      new Date().toString(),
      this.form.get('information.imageUrl').value,
      this.form.get('information.imageSrcBlob').value,
      this.form.get('information.url').value
    );
  }
}
