import { TestBed } from '@angular/core/testing';

import { StarShipsService } from './starships.service';
import { CacheService } from 'src/app/services/cache/cache.service';
import { StarShipsDataService } from 'src/app/services/starships-data/starships-data.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { StarShipsDataServiceMock } from 'src/assets/mocks/starships/starships-data.service.mock';
import { STARSHIPS_DATA_SERVICE_PAGINATED } from 'src/assets/mocks/starships/starships-data.mock';

describe('StarShipsService', () => {
  let service: StarShipsService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        StarShipsService,
        { provide: StarShipsDataService, useClass: StarShipsDataServiceMock},
        CacheService]
    });
    service = TestBed.inject(StarShipsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
  it('should getAllStarShips', () => {
    service.getAllStarShips();
    service.starShips$.subscribe(response => {
      expect(response).toBeTruthy();
      expect(response.length).toBe(1);
    });
    service.ngOnDestroy();
  });

  it('should addStarShip', () => {
    service.addStarShip(STARSHIPS_DATA_SERVICE_PAGINATED.results[0])
    .subscribe(result => {
      expect(result).toBe(STARSHIPS_DATA_SERVICE_PAGINATED.results[0]);
    });
  });
});
