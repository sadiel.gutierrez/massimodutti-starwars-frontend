import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StarShipsComponent } from './starships.component';


const routes: Routes = [
  { path: '', component: StarShipsComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StarShipsRoutingModule { }
