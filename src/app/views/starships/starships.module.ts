import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StarShipsRoutingModule } from './starships-routing.module';
import { StarShipsComponent } from './starships.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { StarShipCardComponent } from './components/starship-card/starship-card.component';
import { StarShipInformationComponent } from './components/starship-information/starship-information.component';
import { AddEditStarShipComponent } from './components/add-edit-starship/add-edit-starship.component';
import { MaterialModule } from 'src/app/material/material.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';


@NgModule({
  declarations: [
    StarShipsComponent,
    StarShipCardComponent,
    StarShipInformationComponent,
    AddEditStarShipComponent
  ],
  imports: [
    CommonModule,
    StarShipsRoutingModule,
    SharedModule,
    MaterialModule,
    ReactiveFormsModule,
    FormsModule,
    NgxPaginationModule
  ],
  exports: [
    StarShipInformationComponent
  ],
  entryComponents: [
    AddEditStarShipComponent
  ]
})
export class StarShipsModule { }
