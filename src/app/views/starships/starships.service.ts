import { Injectable, OnDestroy } from '@angular/core';
import { Subject, BehaviorSubject, Observable, of  } from 'rxjs';
import { StarShip } from 'src/app/core/models/starship.model';
import { StarShipsDataService } from 'src/app/services/starships-data/starships-data.service';
import { takeUntil } from 'rxjs/operators';
import { Paginated } from 'src/app/core/models/pagination.model';
import { environment } from 'src/environments/environment';

@Injectable()
export class StarShipsService implements OnDestroy {

  /**
   * Use to destroy and prevent memory leaks
   */
  private destroy$: Subject<void> = new Subject<void>();

  /**
   * Contains the list of notifications
   */
  starShips: BehaviorSubject<StarShip[]> = new BehaviorSubject<StarShip[]>([]);
  starShips$ = this.starShips.asObservable();

  /**
   *  Default Page
   */
  defaultPage = 1;

  /**
   *  Current Page
   */
  currentPage = 1;

  /**
   *  Items Per Page
   */
  itemsPerPage = 10;

  /**
   *  Total items
   */
  totalItems = 0;

  /**
   * Variable that holds the subscription that gets the starships count to place in cache
   */
  starShipsCount: BehaviorSubject<any> = new BehaviorSubject<any>(null);
  starShipsCount$ = this.starShipsCount.asObservable();

  constructor(
    private starShipsDataService: StarShipsDataService
  ) {
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  /**
   * Get all starships
   */
  getAllStarShips(page?: number): void {
      this.starShipsDataService.getAllStarShips(page !== undefined ? page : this.defaultPage, { cacheMins: 5 })
      .pipe(
        takeUntil(this.destroy$)
      ).subscribe((response: Paginated<StarShip>) => {
        const starships = [];
        if (response.results) {
          response.results.forEach((starShip: StarShip) => {
            starShip.imageUrl =
             `${environment.api_imageUrl_base}${starShip.url.split('/')[starShip.url.split('/').length - 2]}.jpg`;
            starships.push(starShip);
          });
          this.starShipsCount.next({count: response.count, updated: new Date().toString()});
          this.totalItems = response.count;
          this.starShips.next(starships);
        }
      });
    }

  /**
   * Add New StarShip
   * @param StarShip StarShip to add
   */
  addStarShip(starShip: StarShip): Observable<StarShip> {
    return of(starShip);
  }

  /**
   * Edit StarShip
   * @param StarShip StarShip to edit
   */
  editStarShip(starShip: StarShip): Observable<any> {
    return of(starShip);
  }
}
