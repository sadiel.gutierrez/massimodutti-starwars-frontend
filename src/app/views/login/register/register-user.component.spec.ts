import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from 'src/app/material/material.module';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { AuthenticationGuard } from 'src/app/core/components/guards/authentication/authentication.guard';
import { AuthenticationServiceMock } from 'src/assets/mocks/authentication/authentication-service';
import { AuthenticationService } from 'src/app/services/authentication/authentication.service';
import { MainRegisterUserComponent } from './register-user.component';

describe('MainRegisterUserComponent', () => {
  let component: MainRegisterUserComponent;
  let fixture: ComponentFixture<MainRegisterUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        BrowserAnimationsModule,
        ReactiveFormsModule,
        MaterialModule,
        HttpClientTestingModule,
        RouterTestingModule
      ],
      declarations: [
        MainRegisterUserComponent
      ],
      providers: [
        AuthenticationGuard,
        { provide: AuthenticationService, useClass: AuthenticationServiceMock }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainRegisterUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
