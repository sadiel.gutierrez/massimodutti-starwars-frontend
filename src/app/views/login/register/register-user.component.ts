import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, FormArray, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/services/authentication/authentication.service';
import { appAnimations } from 'src/app/core/animations/animations';
import { User } from 'src/app/core/models/user.model';
import { isValidForm } from 'src/app/core/components/helpers/functions';
import { UsersService } from 'src/app/services/users-service/users.service';

@Component({
  selector: 'app-login-register-user',
  templateUrl: './register-user.component.html',
  styleUrls: ['./register-user.component.scss'],
  animations: appAnimations
})
export class MainRegisterUserComponent implements OnInit {

  /**
   * Contains registration form
   */
  registerUserForm: FormGroup;

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    public authenticationService: AuthenticationService,
    private usersService: UsersService) { }

  ngOnInit() {
    this.registerUserForm = this.initForm();
  }

  /**
   * This is method is used to register the user data
   */
  registerUser(): void {
    if (isValidForm(this.registerUserForm)) {
        const newUser: User = {
          id: null,
          firstName: this.registerUserForm.get('firstName').value,
          lastName: this.registerUserForm.get('lastName').value,
          userName: this.registerUserForm.get('userName').value,
          password: this.registerUserForm.get('password').value,
          roles: this.registerUserForm.get('roles').value
        };
        this.usersService.createNewUser(newUser).subscribe(result => {
          if (result) {
              this.authenticationService.setUser(newUser);
              this.goToHome();
            } else {
              this.setWrongCredentialsError();
            }
          },
          error => {
            this.setWrongCredentialsError();
          }
        );
      }
  }

  /**
   * Method to init form
   */
  private initForm(): FormGroup {

    return this.formBuilder.group({
      userName: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required),
      firstName: new FormControl('', Validators.required),
      lastName: new FormControl('', Validators.required),
      roles: new FormControl(null, Validators.required)
    });
  }

  /**
   * This function is used to redirect the user's logged to the dashboard
   */
  private goToHome(): void {
    this.router.navigate(['/starships']);
  }

  /**
   * This function is used to set the wrong credentials error
   */
  private setWrongCredentialsError(): void {
    this.registerUserForm.setErrors({ 'wrong-credentials': true });
  }

  /**
   * This function is used to redirect the user to the login page form
   */
  public goToLoginPage(): void {
    this.router.navigate(['/login']);
  }

}
