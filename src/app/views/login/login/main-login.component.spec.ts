import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MainLoginComponent } from './main-login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from 'src/app/material/material.module';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { AuthenticationGuard } from 'src/app/core/components/guards/authentication/authentication.guard';
import { AuthenticationServiceMock } from 'src/assets/mocks/authentication/authentication-service';
import { AuthenticationService } from 'src/app/services/authentication/authentication.service';
import { USER } from 'src/assets/mocks/authentication/authentication-service.mock';
import { of } from 'rxjs/internal/observable/of';

describe('MainLoginComponent', () => {
  let component: MainLoginComponent;
  let fixture: ComponentFixture<MainLoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        BrowserAnimationsModule,
        ReactiveFormsModule,
        MaterialModule,
        HttpClientTestingModule,
        RouterTestingModule
      ],
      declarations: [
        MainLoginComponent
      ],
      providers: [
        AuthenticationGuard,
        { provide: AuthenticationService, useClass: AuthenticationServiceMock }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainLoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should call login and gohome', () => {
    component.ngOnInit();

    component.loginForm.setValue({
      userName: 'user1',
      password: '123'
    });

    fixture.detectChanges();

    component.login();
  });

  it('should call login and wrong credentials', () => {

    const authService = TestBed.inject(AuthenticationService);
    spyOn(authService, 'login').and.returnValue(of(false));

    component.ngOnInit();

    component.loginForm.setValue({
      userName: USER.userName,
      password: '123'
    });

    fixture.detectChanges();

    component.login();
  });
});
