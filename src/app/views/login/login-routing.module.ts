import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainLoginComponent } from './login/main-login.component';
import { MainRegisterUserComponent } from './register/register-user.component';

const routes: Routes = [
  {
    path: '',
    component: MainLoginComponent
  },
  {
    path: 'register',
    component: MainRegisterUserComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LoginRoutingModule { }
