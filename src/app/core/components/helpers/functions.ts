import { FormGroup } from '@angular/forms';

/**
 * Iterate on all form and touch controls to show errors
 * @param formGroup Form group to validate
 */
export function validateAllFormFields(formGroup: FormGroup): void {
    formGroup.markAllAsTouched();
}

/**
 * Compare selected object with option object
 * @param prop Object property to evaluate
 * @param c1 Object origin to compare
 * @param c2 Object destiny to compare
 */
export function compareWith(prop: string, c1: any, c2: any): boolean {
    return c1 && c2 ? c1[prop] === c2[prop] : c1 === c2;
}

/**
 * Parse date to format YYYY/MM/DD
 * @param date Date to format
 */
export function formatDate(date) {
    if (date) {
        const d = new Date(date);
        let month = '' + (d.getMonth() + 1);
        let day = '' + d.getDate();
        const year = d.getFullYear();

        if (month.length < 2) { month = '0' + month; }
        if (day.length < 2) { day = '0' + day; }

        return [year, month, day].join('-');
    } else {
        return null;
    }
}

/**
 * Touch all controls and return if form is valid or not
 * @param formGroup Form group to evaluate
 */
export function isValidForm(formGroup: FormGroup): boolean {
    validateAllFormFields(formGroup);
    return formGroup.valid;
}
