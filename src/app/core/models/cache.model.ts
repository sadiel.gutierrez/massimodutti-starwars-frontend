export class HttpOptions {
    cacheMins?: number;
}

export class LocalStorageSaveOptions {
    key: string;
    data: any;
    expirationMins?: number;
}
