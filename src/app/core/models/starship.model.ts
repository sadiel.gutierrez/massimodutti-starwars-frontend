export class StarShip {

    constructor(
        public id?: number,
        public name?: string,
        public model?: string,
        public manufacturer?: string,
        public passengers?: number,
        public length?: string,
        public created?: string,
        public imageUrl?: string,
        public imageSrcBlob?: any,
        public url?: string
    ) {

    }
}
