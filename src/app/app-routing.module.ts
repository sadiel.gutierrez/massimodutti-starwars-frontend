import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutDashboardComponent } from './layout/layout-dashboard/layout-dashboard.component';
import { AuthenticationGuard } from './core/components/guards/authentication/authentication.guard';
import { LayoutBasicComponent } from './layout/layout-basic/layout-basic.component';
import { NavigationGuard } from './core/components/guards/navigation/navigation.guard';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'starships',
    pathMatch: 'full'
  },
  {
    path: 'login',
    loadChildren: () => import('./views/login/login.module').then(m => m.LoginModule),
  },
  {
    path: '',
    component: LayoutDashboardComponent,
    children: [
      {
        path: 'home',
        loadChildren: () => import('./views/home/home.module').then(m => m.HomeModule),
        canActivate: [AuthenticationGuard]
      },
      {
        path: 'administration',
        loadChildren: () => import('./views/home/home.module').then(m => m.HomeModule),
        canActivate: [AuthenticationGuard, NavigationGuard],
        data: {
          roles: ['ROLE_ADMIN']
        }
      },
      {
        path: 'starships',
        loadChildren: () => import('./views/starships/starships.module').then(m => m.StarShipsModule),
        canActivate: [AuthenticationGuard]
      }
    ]
  },
  {
    path: '',
    component: LayoutBasicComponent,
    children: [
      {
        path: 'error/:error',
        loadChildren: () => import('./views/error/error.module').then(m => m.ErrorModule),
      }
    ]
  },
  {
    path: '**',
    redirectTo: '/error/404'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
