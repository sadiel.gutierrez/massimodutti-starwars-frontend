import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Paginated } from 'src/app/core/models/pagination.model';
import { Observable, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { StarShip } from 'src/app/core/models/starship.model';
import { DataService } from '../data-service/data-service.service';
import { CacheService } from '../cache/cache.service';
import { switchMap } from 'rxjs/operators';
import { HttpOptions } from 'src/app/core/models/cache.model';

@Injectable()
export class StarShipsDataService {

  constructor(
    private httpClient: HttpClient,
    private dataService: DataService,
    private cacheService: CacheService
  ) {
  }

  /**
   * Get all starships in paginated format
   */
  getAllStarShips(page: number, options: HttpOptions): Observable<Paginated<StarShip>> {

    const url = `${environment.api_url_base}/starships/?page=${page}`;

    options.cacheMins = options.cacheMins || 0;

    if (options.cacheMins > 0) {

        const data = this.cacheService.load(url);

        if (data !== null) {
            return of<Paginated<StarShip>>(data);
        }
    }

    if (environment.api_offline) {
      return this.dataService.get(`starships-${page}`);
    } else {
      return this.httpClient.get<Paginated<StarShip>>(url)
      .pipe(
        switchMap(response => {
            if (options.cacheMins > 0) {

                this.cacheService.save({
                    key: url,
                    data: response,
                    expirationMins: options.cacheMins
                });
            }
            return of<Paginated<StarShip>>(response);
        })
      );
    }
  }
}
