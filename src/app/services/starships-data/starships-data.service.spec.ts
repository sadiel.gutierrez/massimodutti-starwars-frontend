import { TestBed } from '@angular/core/testing';

import { StarShipsDataService } from './starships-data.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { environment } from 'src/environments/environment';
import { CacheService } from '../cache/cache.service';
import { STARSHIPS_DATA_SERVICE_PAGINATED } from 'src/assets/mocks/starships/starships-data.mock';

describe('StarShipsDataService', () => {
  let httpTestingController: HttpTestingController;
  let service: StarShipsDataService;
  let cacheService: CacheService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        StarShipsDataService,
        CacheService
      ]
    });
    httpTestingController = TestBed.inject(HttpTestingController);
    service = TestBed.inject(StarShipsDataService);
    cacheService = TestBed.inject(CacheService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should getAllStarShips when api is offline', () => {
    environment.api_offline = true;
    service.getAllStarShips(1, {}).subscribe(response => {
      expect(response.count).toBe(1);
    });
  });

  it('should getAllStarShips when api is online and no cache is configured', () => {
    environment.api_offline = false;
    const page = 1;
    service.getAllStarShips(page, {}).subscribe(response => {
      expect(response).toBe(STARSHIPS_DATA_SERVICE_PAGINATED);
    });
    const request = httpTestingController.expectOne(`${environment.api_url_base}/starships/?page=${page}`);
    request.flush(STARSHIPS_DATA_SERVICE_PAGINATED);
    httpTestingController.verify();
  });

  it('should getAllStarShips when api is online and cache doesnt exists', () => {
    environment.api_offline = false;
    const page = 1;

    spyOn(cacheService, 'save').and.callThrough();

    service.getAllStarShips(page, { cacheMins: 5 }).subscribe(response => {
      expect(response).toBeTruthy();
    });

    httpTestingController.expectOne(`${environment.api_url_base}/starships/?page=${page}`);

    httpTestingController.verify();
  });

  it('should getAllStarShips when api is online and cache exists', () => {
    environment.api_offline = false;
    const page = 1;

    spyOn(cacheService, 'load').and.callThrough().and.returnValue({} as any);

    service.getAllStarShips(page, { cacheMins: 5 }).subscribe(response => {
      expect(response).toBeTruthy();
    });

    httpTestingController.expectNone(`${environment.api_url_base}/starships/?page=${page}`);

    httpTestingController.verify();
  });
});
