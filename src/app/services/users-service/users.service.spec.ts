import { TestBed } from '@angular/core/testing';

import { UsersService } from './users.service';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import { USER } from 'src/assets/mocks/authentication/authentication-service.mock';
import { JsonPipe } from '@angular/common';

let httpTestingController: HttpTestingController;

describe('UsersService', () => {
  let service: UsersService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
    });

    httpTestingController = TestBed.inject(HttpTestingController);
    service = TestBed.inject(UsersService);
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should check if userExists and return false', () => {
    service.usersRegistrations.next([]);
    const result = service.userExists(USER);
    expect(result).toBe(false);
  });

  it('should check if userExists and return true', () => {
    service.usersRegistrations.next([USER]);
    const result = service.userExists(USER);
    expect(result).toBe(true);
  });

  it('should create new User', () => {
    service.usersRegistrations.next([]);
    service.createNewUser(USER).subscribe(response => {
      expect(response).toBe(true);
      expect(service.userExists(USER)).toBe(true);
    });
  });

  it('should not create new User when user already exists', () => {
    service.createNewUser(USER);
    service.createNewUser(USER).subscribe(response => {
      expect(response).toBe(false);
    });
  });
});
