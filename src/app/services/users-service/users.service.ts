import { Injectable } from '@angular/core';
import { User } from 'src/app/core/models/user.model';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { AUTH_USER_CREDENTIALS_STORAGE, AUTH_USERS_STORAGE } from 'src/app/core/models/constants.model';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  /**
   * User Registrations from Local Storage
   */
  public usersRegistrations: BehaviorSubject<User[]> = new BehaviorSubject(null);
  public usersRegistrations$ = this.usersRegistrations.asObservable();

  constructor( ) {
    this.loadAvailableUsersFromLocalStorage();
   }

  loadAvailableUsersFromLocalStorage() {
    const availableUsers = JSON.parse(localStorage.getItem(AUTH_USERS_STORAGE));
    if (availableUsers) {
      this.usersRegistrations.next(availableUsers);
    }
  }

  userExists(user: User) {
    const foundUser = this.usersRegistrations.getValue()?.find(x => x.userName === user.userName);
    if (foundUser) {
      return true;
    } else {
      return false;
    }
  }

  createNewUser(user: User): Observable<boolean> {
    if (!this.userExists(user)) {
      localStorage.setItem(AUTH_USER_CREDENTIALS_STORAGE, JSON.stringify(user));
      let availableUsers = this.usersRegistrations.getValue();
      if (availableUsers === null) {
        availableUsers = [];
      }
      availableUsers.push(user);
      localStorage.setItem(AUTH_USERS_STORAGE, JSON.stringify(availableUsers));
      this.usersRegistrations.next(availableUsers);
      return of(true);
    }
    return of(false);
  }
}
