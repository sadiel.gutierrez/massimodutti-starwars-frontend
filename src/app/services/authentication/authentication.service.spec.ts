import { TestBed } from '@angular/core/testing';

import { AuthenticationService } from './authentication.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClient } from '@angular/common/http';
import { USER } from 'src/assets/mocks/authentication/authentication-service.mock';
import { UserLogin } from 'src/app/core/models/user.login.model';
import { UsersService } from '../users-service/users.service';

describe('AuthenticationService', () => {
  let service: AuthenticationService;
  let userService: UsersService;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        RouterTestingModule
      ],
      providers: [
        HttpClient,
        UsersService
      ]
    });
    userService = TestBed.inject(UsersService);
    service = TestBed.inject(AuthenticationService);
  });

  afterEach(() => {
    service.setUser(null);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
  it('should call setUser set user params recibed', () => {
    service.setUser(USER);
    service.user$.subscribe(user => {
    expect(user.userName).toBe('massimodutti');
    });
  });
  it('should call userCan without user', () => {
    service.setUser(null);
    service.userCan(['ROLE_ADMIN']).subscribe(can => {
      expect(can).toBeFalsy();
    });
  });
  it('should call userCan with user', () => {
    userService.createNewUser(USER);
    service.setUser(USER);
    service.userCan(['ROLE']).subscribe(can => {
      expect(can).toBe(false);
    });
  });
  it('should call logout', () => {
    service.setUser(USER);
    service.logout();
    service.user$.subscribe(user => {
      expect(user).toBeNull();
    });
  });
  it('should call isAuthenticated and get valid authorized', () => {
    userService.usersRegistrations.next([]);
    userService.createNewUser(USER);
    service.setUser(USER);
    service.isAuthenticated().subscribe(authorized => {
      expect(authorized).toBe(true);
    });
  });
  it('should call isAuthenticated and get unauthorized', () => {
    service.setUser(null);
    spyOn(localStorage, 'getItem').and.returnValue(null);
    service.isAuthenticated().subscribe(authorized => {
      expect(authorized).toBe(false);
    });
  });
  it('should call login and get true', () => {
    userService.usersRegistrations.next([]);
    service.setUser(null);
    userService.createNewUser(USER);
    service.login({userName: 'Massimo', password: '@=e8_R<k9HdU<rdY'} as UserLogin).subscribe(authorized => {
      expect(authorized).toBeTruthy();
    });
  });
});
