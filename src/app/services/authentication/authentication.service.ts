import { Injectable } from '@angular/core';
import { Observable, from, of, BehaviorSubject } from 'rxjs';
import { map, concatMap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { User } from 'src/app/core/models/user.model';
import { UserLogin } from 'src/app/core/models/user.login.model';
import { Role } from 'src/app/core/models/role.model';
import { DataService } from '../data-service/data-service.service';
import { UsersService } from '../users-service/users.service';
import { AUTH_USER_CREDENTIALS_STORAGE } from 'src/app/core/models/constants.model';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  /**
   * Logged user on application
   */
  private user: BehaviorSubject<User> = new BehaviorSubject(null);
  public user$ = this.user.asObservable();

  /**
   * Available user roles
   */
  private roles: BehaviorSubject<Role[]> = new BehaviorSubject(null);
  public roles$ = this.roles.asObservable();

  constructor(
    private router: Router,
    private dataService: DataService,
    private usersService: UsersService
  ) {
    this.dataService.get('user-roles').subscribe(response => {
      this.roles.next(response);
    });
    this.setUser(JSON.parse(localStorage.getItem(AUTH_USER_CREDENTIALS_STORAGE)));
  }

  /**
   * Set logged user
   * @param user user data
   */
  setUser(user: User) {
    this.user.next(user);
  }

  login(userLogin: UserLogin): Observable<boolean> {
    return this.getLoggedInUser()
    .pipe(map(user => {
      if (user) {
        this.router.navigate(['/starships']);
        return true;
      } else {
        const currentUser =
          this.usersService.usersRegistrations.getValue()?.find(x =>
            x.userName === userLogin.userName && x.password === userLogin.password);
        if (currentUser) {
          localStorage.setItem(AUTH_USER_CREDENTIALS_STORAGE, JSON.stringify(currentUser));
          return true;
        }
      }
      return false;
    }));
  }

  /**
   * Get logged in User
   */
  getLoggedInUser(): Observable<any> {
    return of(JSON.parse(localStorage.getItem(AUTH_USER_CREDENTIALS_STORAGE)));
  }

  /**
   * Logout the user
   */
  logout(): void {
    localStorage.removeItem(AUTH_USER_CREDENTIALS_STORAGE);
    this.user.next(null);
    this.router.navigate(['/login']);
  }

  /**
   * Determinates if user can do action or not
   * @param hasPrivilege Privilege to analize
   */
  userCan(hasRoles: string[]): Observable<boolean> {
    return this.user$
      .pipe(map(user => {
        if (!user) { return false; }
        return hasRoles.some(analizeRole => user.roles?.some(currentRole => currentRole.code === analizeRole));
      })
      );
  }

  /**
   * Verify that the user is authenticated
   */
  isAuthenticated(): Observable<boolean> {
    return from(this.getLoggedInUser()
      .pipe(
        concatMap((user) => {
          if (user) {
            this.setUser(user);
            return of(true);
          } else {
            this.router.navigate(['login']);
            return of(false);
          }
        })
      ));
  }

}
