import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TopBarComponent } from './components/top-bar/top-bar.component';
import { MaterialModule } from '../material/material.module';
import { SideNavComponent } from './components/side-nav/side-nav.component';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { UrlSrcDirective } from './components/directives/url-src/url-src.directive';
import { DialogStructureComponent } from './components/dialog-structure/dialog-structure.component';
import { NgxPaginationModule } from 'ngx-pagination';

@NgModule({
  declarations: [
    TopBarComponent,
    SideNavComponent,
    UrlSrcDirective,
    DialogStructureComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    RouterModule,
    ReactiveFormsModule,
    FormsModule,
    FontAwesomeModule,
    NgxPaginationModule
  ],
  exports: [
    TopBarComponent,
    SideNavComponent,
    UrlSrcDirective,
    DialogStructureComponent
  ],
  entryComponents: [
  ]
})
export class SharedModule { }
