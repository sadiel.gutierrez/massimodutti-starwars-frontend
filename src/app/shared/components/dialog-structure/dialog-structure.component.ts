import { Component, OnInit, Input } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-dialog-structure',
  templateUrl: './dialog-structure.component.html',
  styleUrls: ['./dialog-structure.component.scss']
})
export class DialogStructureComponent implements OnInit {
  /**
   * Title to show on dialog
   */
  @Input() title: string;
  /**
   * reference to child component
   */
  @Input() dialogRef: MatDialogRef<any>;
  constructor(
  ) { }

  ngOnInit(): void {
  }

  /**
   * Close dialog without result
   */
  close() {
    this.dialogRef.close();
  }

}
