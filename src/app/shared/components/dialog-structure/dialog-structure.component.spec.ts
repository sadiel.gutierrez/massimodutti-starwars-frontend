import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogStructureComponent } from './dialog-structure.component';
import { MatDialogRef } from '@angular/material/dialog';

describe('DialogStructureComponent', () => {
  let component: DialogStructureComponent;
  let fixture: ComponentFixture<DialogStructureComponent>;
  const dialogMock = {
    close: () => { }
  };
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DialogStructureComponent],
      providers: [
        { provide: MatDialogRef, useValue: dialogMock },
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogStructureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
